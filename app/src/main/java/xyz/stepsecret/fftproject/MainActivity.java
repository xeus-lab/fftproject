package xyz.stepsecret.fftproject;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidplot.Plot;
import com.androidplot.util.PlotStatistics;
import com.androidplot.util.Redrawer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jtransforms.fft.DoubleFFT_1D;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private LineChart mChart;
    private LineChart mWave;

    private RecordingThread mRecordingThread;
    private static final int REQUEST_RECORD_AUDIO = 13;

    private FloatingActionButton fab;
    private FloatingActionButton fab_connect;
    private FloatingActionButton fab_receive;
    private FloatingActionButton fab_save;

    private Client socket;

    private Boolean check_connect = false;
    Boolean check_receive = false;

    private EditText edt_ip;
    private EditText edt_port;
    private EditText edt_filename;

    private int size_data = 2048;
    private short[] store_data = new short[size_data];
    private int count_data = 0;
    private JSONArray store_message_json ;

    private static final int HISTORY_SIZE = 1000;
    private XYPlot aprHistoryPlot = null;
    private SimpleXYSeries azimuthHistorySeries = null;
    private Redrawer redrawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init1();
        init2();
        init3();

        mRecordingThread = new RecordingThread(new AudioDataReceivedListener() {
            @Override
            public void onAudioDataReceived(final short[] data) {

                // redraw
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mChart.resetTracking();
                        mWave.resetTracking();

                        int size = data.length;
                        float samples[] = new float[size];
                        short[] buffer = new short[size];
                        int frequency = 2200;
                        int frequency2 = 1100;

                        for (int i = 0; i < size; i++) {
                            samples[i] = (float) Math.sin( (float)i * ((float)(2*Math.PI) * frequency / 44100));  //the part that makes this a sine wave....
                            //samples[i] = samples[i] +1;
                            //Log.e(" >>>> "," samples[i]  : "+samples[i] );
                            //float x = (float) Math.sin( (float)i * ((float)(2*Math.PI) * frequency2 / 44100));  //the part that makes this a sine wave....
                            buffer[i] = (short) (samples[i] * Short.MAX_VALUE);
                           //Log.e(" >>>> "," buffer[i]  : "+buffer[i] );
                        }

                        // from record
                        show1(data);

                        // from generate sine wave
                        //show2(buffer);

                        //stuff that updates ui
                        mChart.invalidate();
                        mWave.invalidate();
                    }
                });
            }



        });


        edt_ip = (EditText) findViewById(R.id.edt_ip);
        edt_port = (EditText) findViewById(R.id.edt_port);
        edt_filename = (EditText) findViewById(R.id.edt_filename);


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mRecordingThread.recording()) {
                    startAudioRecordingSafe();
                } else {
                    mRecordingThread.stopRecording();
                }
            }
        });

        fab_connect = (FloatingActionButton) findViewById(R.id.fab_connect);
        fab_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!check_connect)
                {
                    count_data = 0;

                    socket = new Client(edt_ip.getText().toString(), Integer.parseInt(edt_port.getText().toString()));
                    socket.setClientCallback(new Client.ClientCallback () {

                        @Override
                        public void onMessage(String message) {
                           // store_message_json = message;
                            Log.e(" Main ",":> "+message);

                            try {



                                JSONObject jsonObj = new JSONObject(message);

                                JSONArray Data = jsonObj.getJSONArray("data");
                                store_message_json = jsonObj.getJSONArray("data");

                                //Log.e(" Main "," Data : "+Data.getJSONArray(0).get(0));

                                 final short[] temp = new short[Data.length()];

                                for(int i = 0 ; i < Data.length(); i++)
                                {
                                    //Log.e(" Main ",i+" : Data : "+Data.getJSONArray(i).get(0));

                                    temp[i] = Short.parseShort(Data.getJSONArray(i).get(0).toString());
                                }

                                // redraw
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mChart.resetTracking();
                                        mWave.resetTracking();


                                        show1(temp);

                                        //stuff that updates ui
                                        mChart.invalidate();
                                        mWave.invalidate();
                                    }
                                });

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e(" Main "," fail "+e);
                            }

                            /*String[] parts = message.split(",");

                            store_data[count_data] = Short.parseShort(parts[0]);
                            count_data++;

                            //Log.e(" Main ",":> "+Short.parseShort(parts[0]));

                            /////////////////////
                            // get rid the oldest sample in history:
                            if (azimuthHistorySeries.size() > HISTORY_SIZE) {
                                azimuthHistorySeries.removeFirst();

                            }

                            // add the latest history sample:
                            azimuthHistorySeries.addLast(null, Short.parseShort(parts[0]));
                            /////////////////////

                           if(count_data == size_data)
                            {
                                // disconnect if size full
                               // check_connect = false;
                               // socket.disconnect();

                                final short[] temp = store_data;
                                count_data = 0;

                                // redraw
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mChart.resetTracking();
                                        mWave.resetTracking();


                                        show1(temp);

                                        //stuff that updates ui
                                        mChart.invalidate();
                                        mWave.invalidate();
                                    }
                                });
                            }
                            */

                        }

                        @Override
                        public void onConnect(Socket socket) {
                            //socket.send("Hello World!\n");
                            //socket.disconnect();
                            runOnUiThread(new Runnable() {
                                public void run()
                                {
                                    Toast.makeText(getApplicationContext(), "onConnect",
                                            Toast.LENGTH_LONG).show();
                                }
                            });

                        }

                        @Override
                        public void onDisconnect(Socket socket, String message) {
                            runOnUiThread(new Runnable() {
                                public void run()
                                {
                                    Toast.makeText(getApplicationContext(), "onDisconnect",
                                            Toast.LENGTH_LONG).show();
                                }
                            });
                        }

                        @Override
                        public void onConnectError(Socket socket, String message) {
                            runOnUiThread(new Runnable() {
                                public void run()
                                {
                                    Toast.makeText(getApplicationContext(), "onConnectError",
                                            Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });

                    check_connect = true;
                    socket.connect();
                }
                else
                {
                    count_data = 0;
                    check_connect = false;
                    socket.disconnect();
                }

            }
        });


        fab_receive = (FloatingActionButton) findViewById(R.id.fab_receive);
        fab_receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!check_receive)
                {
                    check_receive = true;
                    socket.send("start");
                }
                else
                {
                    check_receive = false;
                    socket.send("exit");
                }

            }
        });

        fab_save = (FloatingActionButton) findViewById(R.id.fab_save);
        fab_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                writeToFile(store_message_json,getApplicationContext());

            }
        });





    }


    @Override
    public void onResume() {
        super.onResume();
        redrawer.start();
    }

    @Override
    public void onPause() {
        redrawer.pause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        redrawer.finish();
        super.onDestroy();
    }

    private void writeToFile(JSONArray data, Context context) {
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = df.format(Calendar.getInstance().getTime());

        //edt_filename.getText().toString()+"_"+date+".json"

        // create a File object for the parent directory
        File wallpaperDirectory = new File(Environment.getExternalStorageDirectory().toString()+"/FFT Project/");
// have the object build the directory structure, if needed.
        wallpaperDirectory.mkdirs();

        File file;
        FileOutputStream outputStream;
        try {
            file = new File(Environment.getExternalStorageDirectory(), "FFT Project/"+edt_filename.getText().toString()+"_"+date+".json");

            outputStream = new FileOutputStream(file);
            for(int i = 0 ; i < data.length(); i++)
            {
                //Log.e(" Main ",i+" : Data : "+Data.getJSONArray(i).get(0));

                //data.getJSONArray(i).get(0).toString();

                try {
                    String temp = data.getJSONArray(i).get(0).toString()+"''";
                           temp = data.getJSONArray(i).get(1).toString()+"''";
                    outputStream.write(temp.getBytes());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void init1()
    {
        mChart = (LineChart) findViewById(R.id.chart1);
        mChart.setDrawGridBackground(false);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);


        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);


        mChart.getAxisRight().setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(45);
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {


                //return (int)(value * 1000 * 44100 / 2048 )+" hz"; // Class FFT
                return (int)(value * 1000 * 2600 / 11000 /2)+" hz"; // Lib jtransforms
                // 447 is samplngrate
            }
        });



        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        //leftAxis.setAxisMaximum(26000f);
        leftAxis.setAxisMaximum(500f);
        leftAxis.setAxisMinimum(0);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawGridLines(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        // dont forget to refresh the drawing
        mChart.invalidate();
    }

    private void init2()
    {
        mWave = (LineChart) findViewById(R.id.wave);
        mWave.setDrawGridBackground(false);

        // no description text
        mWave.getDescription().setEnabled(false);

        // enable touch gestures
        mWave.setTouchEnabled(true);

        // enable scaling and dragging
        mWave.setDragEnabled(true);
        mWave.setScaleEnabled(true);


        // if disabled, scaling can be done on x- and y-axis separately
        mWave.setPinchZoom(false);


        mWave.getAxisRight().setEnabled(false);

        XAxis xAxis = mWave.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelRotationAngle(45);
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {


                return (int)(value*1000)+"";
            }
        });



        YAxis leftAxis = mWave.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        //leftAxis.setAxisMaximum(16000f);
       // leftAxis.setAxisMinimum(-16000f);
        leftAxis.setAxisMaximum(500);
        leftAxis.setAxisMinimum(-500);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawGridLines(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        // dont forget to refresh the drawing
        mWave.invalidate();
    }

    private void init3()
    {
        // setup the APR History plot:
        aprHistoryPlot = (XYPlot) findViewById(R.id.aprHistoryPlot);

        azimuthHistorySeries = new SimpleXYSeries("Az.");
        azimuthHistorySeries.useImplicitXVals();

        aprHistoryPlot.setRangeBoundaries(1, 1000, BoundaryMode.FIXED);
        aprHistoryPlot.setDomainBoundaries(0, HISTORY_SIZE, BoundaryMode.FIXED);
        aprHistoryPlot.addSeries(azimuthHistorySeries,
                new LineAndPointFormatter(
                        Color.rgb(255, 0, 0), null, null, null));

        aprHistoryPlot.setDomainStepMode(StepMode.INCREMENT_BY_VAL);
        aprHistoryPlot.setDomainStepValue(HISTORY_SIZE/5);
        aprHistoryPlot.setLinesPerRangeLabel(2);
        aprHistoryPlot.setDomainLabel("Sample Index");
        aprHistoryPlot.getDomainTitle().pack();
        aprHistoryPlot.setRangeLabel("RAW");
        aprHistoryPlot.getRangeTitle().pack();

        aprHistoryPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).
                setFormat(new DecimalFormat("#"));

        aprHistoryPlot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).
                setFormat(new DecimalFormat("#"));

        //final PlotStatistics histStats = new PlotStatistics(1000, false);

        //aprHistoryPlot.addListener(histStats);

        redrawer = new Redrawer(
                Arrays.asList(new Plot[]{aprHistoryPlot}),
                100, false);
    }

    // Jtransform
    private void show1(short[] data)
    {
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        ArrayList<Entry> yVals_wave = new ArrayList<Entry>();

        int size = data.length;
        double[] temp_data = new double[size*2];

        temp_data[1] = size/2;
        for (int i = 0; i < size; i++) {

            temp_data[2*i] = (double) data[i] / 32768.0;
            temp_data[2*i+1] = 0;

            yVals_wave.add(new Entry(i * 0.001f, (float) data[i]));
        }
        DoubleFFT_1D fft = new DoubleFFT_1D(size);
        fft.complexForward(temp_data);

        int new_size = size ;
        short[] data_temp = new short[new_size];
        short max_amp = 0;
        int num = 0;
        for(int i = 0 ; i < new_size ;  i += 2)
        {

            data_temp[i] = (short)  (Math.sqrt(temp_data[i] * temp_data[i] + temp_data[i + 1] * temp_data[i + 1])*100);
            yVals.add(new Entry(i * 0.001f, (float) data_temp[i]));

            //Log.e(" Main ","i : "+i+" > FFT : "+ data_temp[i]);
            if(max_amp < data_temp[i])
            {
                max_amp = data_temp[i];
                num = i;
            }
        }
       // Log.e(">> ","max_amp : "+max_amp+" freq : "+(num * 44100 / 1792 )+" hz");
        //Log.e(">> ","size : "+size);


        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "DataSet 1");
        LineDataSet set2 = new LineDataSet(yVals_wave, "DataSet 2");

        set1.setColor(Color.BLACK);
        set1.setLineWidth(0.5f);
        set1.setDrawValues(false);
        set1.setDrawCircles(false);
        set1.setMode(LineDataSet.Mode.LINEAR);
        set1.setDrawFilled(false);

        set2.setColor(Color.BLACK);
        set2.setLineWidth(0.5f);
        set2.setDrawValues(false);
        set2.setDrawCircles(false);
        set2.setMode(LineDataSet.Mode.LINEAR);
        set2.setDrawFilled(false);

        // create a data object with the datasets
        LineData data1 = new LineData(set1);
        LineData data2 = new LineData(set2);

        // set data
        mChart.setData(data1);
        mWave.setData(data2);

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();
        l.setEnabled(false);

        Legend l2 = mWave.getLegend();
        l2.setEnabled(false);

    }

    // class FFT
    private void show2(short[] data)
    {
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        ArrayList<Entry> yVals_wave = new ArrayList<Entry>();

        int size = data.length;
        double[] real = new double[size];
        double[] image = new double[size];

        for (int i = 0; i < size; i++) {

            real[i] = (double) data[i] / 32768.0; // signed 16 bit
            //real[i] = (double) data[i]; // signed 16 bit
            image[i] = 0.0;
            yVals_wave.add(new Entry(i * 0.001f, (float) data[i]));
        }
        //Log.e(">> ","1 real[0] : "+real[0]);
        FFT.transform(real, image);
        //Log.e(">> ","2 real[0] : "+real[0]);

        int new_size = size ;
        short[] data_temp = new short[new_size];
        short max_amp = 0;
        int num = 0;
        for(int i = 0 ; i < new_size ; i++)
        {

            data_temp[i] = (short) (Math.sqrt(real[i]*real[i] + image[i]*image[i]) * 100); // Enlarge 100 times
            yVals.add(new Entry(i * 0.001f, (float) data_temp[i]));
            if(max_amp < data_temp[i])
            {
                max_amp = data_temp[i];
                num = i;
            }
        }
        //Log.e(">> ","max_amp : "+max_amp+" freq : "+(num * 44100 / 1792 )+" hz");
        //Log.e(">> ","size : "+size);


        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "DataSet 1");
        LineDataSet set2 = new LineDataSet(yVals_wave, "DataSet 2");

        set1.setColor(Color.BLACK);
        set1.setLineWidth(0.5f);
        set1.setDrawValues(false);
        set1.setDrawCircles(false);
        set1.setMode(LineDataSet.Mode.LINEAR);
        set1.setDrawFilled(false);

        set2.setColor(Color.BLACK);
        set2.setLineWidth(0.5f);
        set2.setDrawValues(false);
        set2.setDrawCircles(false);
        set2.setMode(LineDataSet.Mode.LINEAR);
        set2.setDrawFilled(false);

        // create a data object with the datasets
        LineData data1 = new LineData(set1);
        LineData data2 = new LineData(set2);

        // set data
        mChart.setData(data1);
        mWave.setData(data2);

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();
        l.setEnabled(false);

        Legend l2 = mWave.getLegend();
        l2.setEnabled(false);

    }

    @Override
    protected void onStop() {
        super.onStop();

        mRecordingThread.stopRecording();
    }


    private void startAudioRecordingSafe() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {
            mRecordingThread.startRecording();
        } else {
            requestMicrophonePermission();
        }
    }

    private void requestMicrophonePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.RECORD_AUDIO)) {
            // Show dialog explaining why we need record audio
            Snackbar.make(mChart, "Microphone access is required in order to record audio",
                    Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                            android.Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO);
                }
            }).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    android.Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_RECORD_AUDIO && grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mRecordingThread.stopRecording();
        }
    }
}

